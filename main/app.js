function addTokens(input, tokens){

    if(typeof input !== 'string'){
        throw new Error(`Invalid input`);
    }
    
    if(input.length<6){
        throw new Error(`Input should have at least 6 characters`);
    }

    for(let i=0; i<tokens.length; i++){

        substring="...";

        if(input.includes(substring)){
            input = input.replace(substring, '${' + tokens[i].tokenName + '}');
        }

        if(typeof tokens[i].tokenName !== 'string'){
            throw new Error('Invalid array format');
        }
            
        return input;
        
    }

}

const app = {
    addTokens: addTokens
}

module.exports = app;